# Redis data migrator tool for NEON-dX #

This tool can be used to migrate the data of a NEON-dX redis instance to another. 

## Build steps

1. Clone the repository.
2. From the root directory of repository run following command

```bash
mvn clean install
```

## Execution steps

Copy the resultant jar file from the build to any of the node in NEON-dX cluster. Then execute the following command from the directory where the jar file is copied 

```bash
java -Dprimary.redis.host=<primary-redis-host> -Dsecondary.redis.host=<secondary-redis-host> -Dprimary.redis.port=<primary-redis-port> -Dsecondary.redis.port=<secondary-redis-port> -Dprimary.redis.password=<primary-redis-password> -Dsecondary.redis.password=<secondary-redis-password> -Dprimary.redis.connection.timeOut=<primary-redis-connection-timeout> -Dsecondary.redis.connection.timeOut=<secondary-redis-connection-timeout> -Dmysql.host=<mysql-host> -Dmysql.port=<mysql.port> -Dmysql.username=<mysql-username> -Dmysql.password=<mysql.password> -cp redis-data-migrator-0.0.1-SNAPSHOT.jar org.springframework.boot.loader.JarLauncher
```