package com.flytxt.redis.scanner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

/**
 * The RedisMigratorApplication class
 *
 * @author sivasyam
 *
 */
@SpringBootApplication
@Slf4j
public class RedisMigratorApplication implements CommandLineRunner {

    private static String primaryRedisHost;

    private static String primaryRedisPort;

    private static String primaryRedisPassword;

    private static String primaryRedisConnectionTimeout;

    private static String secondaryRedisHost;

    private static String secondaryRedisPort;

    private static String secondaryRedisPassword;

    private static String secondaryRedisConnectionTimeout;

    private static String nsnLength;

    private static String crossCheckUniqueEventsWithDB;

    private static boolean crossCheckUniqueEventsWithDBValue = false;

    private static int nsnLengthValue;

    private static String mysqlHost;

    private static int mysqlPort;

    private static String mysqlUsername;

    private static String mysqlPassword;

    private List<Long> eventInstanceIds = new ArrayList<Long>();

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private String urlTemplate = "jdbc:mysql://@HOST@:@PORT@/neon?useUnicode=true&characterEncoding=UTF-8&useSSL=false";

    private static final String GET_EVENT_INSTANCE_IDS_FROM_DB = "SELECT EVENT_INSTANCE_ID FROM app_instance WHERE ((UNIX_TIMESTAMP(SCHEDULED_DATE) * 1000) + (tracking_session_expiry_time * 60000) + (86400000)) > ((UNIX_TIMESTAMP(CURTIME())) * 1000) AND PRODUCT_ID = 2";

    public static void main(String[] args) {

        primaryRedisHost = System.getProperty("primary.redis.host");
        primaryRedisPort = System.getProperty("primary.redis.port");
        primaryRedisPassword = System.getProperty("primary.redis.password");
        primaryRedisConnectionTimeout = System.getProperty("primary.redis.connection.timeOut");

        secondaryRedisHost = System.getProperty("secondary.redis.host");
        secondaryRedisPort = System.getProperty("secondary.redis.port");
        secondaryRedisPassword = System.getProperty("secondary.redis.password");
        secondaryRedisConnectionTimeout = System.getProperty("secondary.redis.connection.timeOut");

        mysqlHost = System.getProperty("mysql.host");
        mysqlPort = Integer.valueOf(System.getProperty("mysql.port")).intValue();
        mysqlUsername = System.getProperty("mysql.username");
        mysqlPassword = System.getProperty("mysql.password");

        nsnLength = System.getProperty("scanNsnLength");
        crossCheckUniqueEventsWithDB = System.getProperty("crossCheckUniqueEventsWithDB");

        log.info("Primary redis connection details, host: {}, port: {}, password: {}, timeout: {}", primaryRedisHost, primaryRedisPort, primaryRedisPassword, primaryRedisConnectionTimeout);
        log.info("Secondary redis connection details, host: {}, port: {}, password: {}, timeout: {}", secondaryRedisHost, secondaryRedisPort, secondaryRedisPassword, secondaryRedisConnectionTimeout);
        log.info("Starting intialization of redis migrator application.........");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException("ERROR: Interrupted the applcation while starting up");
        }

        SpringApplication.run(RedisMigratorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        validateInputDetails();
        if (crossCheckUniqueEventsWithDBValue) {
            eventInstanceIds.clear();
            loadEventInstanceIdsFromDB();
        }

        final Jedis keyScannerJedis = RedisResourceManger.getPrimaryResourceFromPool(primaryRedisHost, Integer.valueOf(primaryRedisPort).intValue(), primaryRedisPassword,
                Integer.valueOf(primaryRedisConnectionTimeout).intValue());
        final Jedis hashScannerJedis = RedisResourceManger.getPrimaryResourceFromPool(primaryRedisHost, Integer.valueOf(primaryRedisPort).intValue(), primaryRedisPassword,
                Integer.valueOf(primaryRedisConnectionTimeout).intValue());
        final Jedis hgetRedis = RedisResourceManger.getPrimaryResourceFromPool(primaryRedisHost, Integer.valueOf(primaryRedisPort).intValue(), primaryRedisPassword,
                Integer.valueOf(primaryRedisConnectionTimeout).intValue());
        final Jedis expiryReadRedis = RedisResourceManger.getPrimaryResourceFromPool(primaryRedisHost, Integer.valueOf(primaryRedisPort).intValue(), primaryRedisPassword,
                Integer.valueOf(primaryRedisConnectionTimeout).intValue());

        final Jedis secondaryJedis = RedisResourceManger.getSecondaryResourceFromPool(secondaryRedisHost, Integer.valueOf(secondaryRedisPort).intValue(), secondaryRedisPassword,
                Integer.valueOf(secondaryRedisConnectionTimeout).intValue());

        final ScanParams keyScanParams = new ScanParams().match("*".getBytes());
        final ScanParams hashScanParams = new ScanParams().match("*".getBytes());
        long totalCount = 0;
        long uniqueEventKeyCount = 0;
        long uniqueEventCount = 0;
        long drCount = 0;
        long drHashCount = 0;
        long errorCount = 0;
        long drsMigrated = 0;
        long uniqueEventsMigrated = 0;
        long daysInDiffMillis = (4 * (1000 * 60 * 60 * 24));
        String keyScanCursor = ScanParams.SCAN_POINTER_START;
        do {
            final ScanResult<String> scanResult = keyScannerJedis.scan(keyScanCursor, keyScanParams);
            final List<String> scanResultList = scanResult.getResult();
            totalCount += scanResultList.size();
            for (final String scanResultElement : scanResultList) {
                if (scanResultElement.contains("DR")) {
                    drHashCount++;
                    final Integer dateIndexPos = (scanResultElement.indexOf("-") + 1);
                    final String drDateString = scanResultElement.substring(dateIndexPos);
                    final Date drDate = DATE_FORMAT.parse(drDateString);
                    if ((System.currentTimeMillis() - drDate.getTime()) <= daysInDiffMillis) {
                        String hashScanCursor = ScanParams.SCAN_POINTER_START;
                        boolean atleastOneDataMigrated = false;
                        do {
                            final ScanResult<Entry<String, String>> hashScanResult = hashScannerJedis.hscan(scanResultElement, hashScanCursor, hashScanParams);
                            List<Entry<String, String>> hashScanResultList = hashScanResult.getResult();

                            for (final Entry<String, String> hashScanResultElement : hashScanResultList) {
                                drCount++;
                                if (secondaryJedis.hset(scanResultElement, hashScanResultElement.getKey(), hashScanResultElement.getValue()) == 1) {
                                    atleastOneDataMigrated = true;
                                    printData(scanResultElement, hashScanResultElement.getKey(), hashScanResultElement.getValue(), -1L);
                                    log.info("DR: {} - Data saved to secondary redis", scanResultElement);
                                    drsMigrated++;
                                } else {
                                    log.info("DR: {} - Data not saved to secondary redis", scanResultElement);
                                }
                            }
                            hashScanCursor = hashScanResult.getStringCursor();
                        } while (!hashScanCursor.equals(ScanParams.SCAN_POINTER_START));

                        if (atleastOneDataMigrated) {
                            final Long dataExpiry = expiryReadRedis.ttl(scanResultElement);
                            if (secondaryJedis.expire(scanResultElement, dataExpiry.intValue()) == 1) {
                                log.info("DR: {} - Expiry set as {}", scanResultElement, dataExpiry);
                            } else {
                                log.info("DR: {} - Expiry is not set", scanResultElement);
                            }
                        }
                    }
                } else {
                    try {
                        final Set<byte[]> hGetResult = hgetRedis.smembers(scanResultElement.getBytes());
                        uniqueEventKeyCount++;
                        for (final byte[] hgetResultElement : hGetResult) {
                            uniqueEventCount++;
                            final String valueElement = new String(hgetResultElement);
                            boolean proceedWithData = true;
                            if (crossCheckUniqueEventsWithDBValue) {
                                final int dataLength = scanResultElement.length();
                                final int endIndex = (dataLength - nsnLengthValue);
                                final Long eventInstanceId = Long.valueOf(scanResultElement.substring(0, endIndex));
                                if (eventInstanceIds.contains(eventInstanceId)) {
                                    proceedWithData = true;
                                } else {
                                    proceedWithData = false;
                                }
                            }
                            if (proceedWithData) {
                                if (secondaryJedis.sadd(scanResultElement, valueElement) == 1) {
                                    final Long dataExpiry = expiryReadRedis.ttl(scanResultElement);
                                    if (secondaryJedis.expire(scanResultElement, dataExpiry.intValue()) == 1) {
                                        log.info("UniqueEvents: {} - Expiry is set as {}", scanResultElement, dataExpiry);
                                    } else {
                                        log.info("UniqueEvents: {} - Expiry is not set", scanResultElement);
                                    }
                                    printData("", scanResultElement, valueElement, dataExpiry);
                                    log.info("UniqueEvents: {} - Data saved to secondary redis", scanResultElement);
                                    uniqueEventsMigrated++;
                                } else {
                                    log.info("UniqueEvents: {} - Data not saved to secondary redis", scanResultElement);
                                }
                            }
                        }
                    } catch (Exception e) {
                        errorCount++;
                        log.info("Failed reading data against key: {}, will be skipped", scanResultElement);
                        printData(scanResultElement, "", "", -1L);
                    }
                }
            }

            keyScanCursor = scanResult.getStringCursor();
        } while (!keyScanCursor.equals(ScanParams.SCAN_POINTER_START));

        log.info(
                "Redis migration completed\n=======================\nTotal scanned: {}\nUnique events scanned: {}\nUnique event keys scanned: {}\nDRs scanned: {}\nDR hashes scanned: {}\nError occured: {}",
                totalCount, uniqueEventCount, uniqueEventKeyCount, drCount, drHashCount, errorCount);
        log.info("\n\n");
        log.info("Migration summary\n===========================\nDRs migrated: {}\nUnique events migrated: {}", drsMigrated, uniqueEventsMigrated);

        keyScannerJedis.close();
        hashScannerJedis.close();
        hgetRedis.close();
        secondaryJedis.close();
    }

    private void printData(String hash, String key, String value, Long expiry) {
        if (expiry != -1L) {
            System.out.println("Hash: " + hash + ", Key: " + key + ", Value: " + value);
        } else {
            System.out.println("Hash: " + hash + ", Key: " + key + ", Value: " + value + ", Expiry: " + expiry);
        }
    }

    @SuppressWarnings("unused")
    private static void validateInputDetails() {

        if (crossCheckUniqueEventsWithDB == null) {
            crossCheckUniqueEventsWithDBValue = false;
            log.info("crossCheckUniqueEventsWithDB is set as false");
        } else if (crossCheckUniqueEventsWithDB.equals("true")) {
            crossCheckUniqueEventsWithDBValue = true;
            log.info("crossCheckUniqueEventsWithDB is set as true");
        } else if (crossCheckUniqueEventsWithDB.equals("false")) {
            crossCheckUniqueEventsWithDBValue = false;
            log.info("crossCheckUniqueEventsWithDB is set as false");
        } else {
            throw new RuntimeException("Invalid crossCheckUniqueEventsWithDB specified");
        }

        if (mysqlHost == null || mysqlHost.isEmpty() || mysqlUsername == null || mysqlUsername.isEmpty() || mysqlPassword == null || mysqlPassword.isEmpty()) {
            throw new RuntimeException("Invalid mysql details specified");
        }

        if (primaryRedisHost == null || primaryRedisHost.isEmpty() || secondaryRedisHost == null || secondaryRedisHost.isEmpty()) {
            throw new RuntimeException("Invalid redis hosts specified");
        }

        if (nsnLength == null || nsnLength.isEmpty()) {
            throw new RuntimeException("Invalid nsn length specified " + nsnLength);
        } else {
            nsnLengthValue = Integer.valueOf(nsnLength).intValue();
        }

        try {
            if (primaryRedisPort == null || primaryRedisPort.isEmpty() || secondaryRedisPort == null || secondaryRedisPort.isEmpty()) {
                try {
                    final Integer primaryRedisServerPort = Integer.valueOf(primaryRedisPort);
                    final Integer secondaryRedisServerPort = Integer.valueOf(secondaryRedisPort);
                } catch (NumberFormatException e) {
                    throw new RuntimeException("Non numeric value for redis server ports specified, error: ", e);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Invalid redis server port specified specified, ERROR: ", e);
        }

        try {
            if (primaryRedisConnectionTimeout == null || primaryRedisConnectionTimeout.isEmpty() || secondaryRedisConnectionTimeout == null || secondaryRedisConnectionTimeout.isEmpty()) {
                try {
                    final Integer primaryRedisServerConnectionTimeout = Integer.valueOf(primaryRedisConnectionTimeout);
                    final Integer secondaryRedisServerConnectionTimeout = Integer.valueOf(secondaryRedisConnectionTimeout);
                } catch (NumberFormatException e) {
                    throw new RuntimeException("Non numeric value for connection timeouts specified, error: ", e);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Invalid redis connection timeout specified, ERROR: ", e);
        }
    }

    private DataSource getDataSource(final String hostName, final int portNum, final String userName, final String passWord) {
        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUsername(userName);
        ds.setPassword(passWord);
        String url = urlTemplate.replace("@HOST@", hostName).replace("@PORT@", "" + portNum);
        ds.setUrl(url);

        ds.setMaxTotal(20);
        ds.setInitialSize(1);
        ds.setDefaultTransactionIsolation(2);
        ds.setValidationQuery("SELECT 1 FROM DUAL");

        ds.setPoolPreparedStatements(false);
        ds.setDefaultAutoCommit(true);
        ds.setAccessToUnderlyingConnectionAllowed(true);
        ds.setMaxWaitMillis(10000);
        return ds;
    }

    private void loadEventInstanceIdsFromDB() {

        final DataSource dataSource = getDataSource(mysqlHost, mysqlPort, mysqlUsername, mysqlPassword);
        try (final Connection con = dataSource.getConnection(); final PreparedStatement getValidEventInstanceIds = con.prepareStatement(GET_EVENT_INSTANCE_IDS_FROM_DB)) {
            try (final ResultSet rs = getValidEventInstanceIds.executeQuery()) {
                while (rs.next()) {
                    final Long eventInstanceId = rs.getLong(1);
                    eventInstanceIds.add(eventInstanceId);
                }
            }

            log.info("\n\n\nLoaded {} valid event instance ids from database.....", eventInstanceIds.size());
            log.info("The migration procedure will start in 30 seconds.........\n\n\n");

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                throw new RuntimeException("ERROR: Interrupted the applcation while starting up");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
