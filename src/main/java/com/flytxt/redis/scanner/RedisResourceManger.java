package com.flytxt.redis.scanner;

import java.net.URISyntaxException;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * The RedisResourceManger class
 *
 * @author sivasyam
 *
 */
@Slf4j
public class RedisResourceManger {

    private static JedisPool primaryJedisPool;

    private static JedisPool secondaryJedisPool;

    private static volatile Object LOCK = new Object();

    public static Jedis getPrimaryResourceFromPool(final String redisServerHost, final int redisServerPort, final String password, final int timeOut) throws URISyntaxException {
        if (primaryJedisPool == null) {
            synchronized (LOCK) {
                if (primaryJedisPool == null) {
                    final JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
                    if (password != null && !password.isEmpty()) {
                        primaryJedisPool = new JedisPool(jedisPoolConfig, redisServerHost, redisServerPort, timeOut, password);
                    } else {
                        primaryJedisPool = new JedisPool(jedisPoolConfig, redisServerHost, redisServerPort, timeOut);
                    }
                    log.info("Initialized primary redis resource pool");
                }
            }
        }
        log.info("Fetched resource from primary redis resource pool");
        return primaryJedisPool.getResource();
    }

    public static Jedis getSecondaryResourceFromPool(final String redisServerHost, final int redisServerPort, final String password, final int timeOut) throws URISyntaxException {
        if (secondaryJedisPool == null) {
            synchronized (LOCK) {
                if (secondaryJedisPool == null) {
                    final JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
                    if (password != null && !password.isEmpty()) {
                        secondaryJedisPool = new JedisPool(jedisPoolConfig, redisServerHost, redisServerPort, timeOut, password);
                    } else {
                        secondaryJedisPool = new JedisPool(jedisPoolConfig, redisServerHost, redisServerPort, timeOut);
                    }
                    log.info("Initialized secondary redis resource pool");
                }
            }
        }
        log.info("Fetched resource from secondary redis resource pool");
        return secondaryJedisPool.getResource();
    }
}
